from .model import Approach, Segment, BikePath, Crossing, \
    Sidewalk, Intersection, RailroadCrossing, Sign
import geopandas as gpd
import subprocess
from progress.bar import Bar
import pandas as pd


class Assessment(object):
    def __init__(self, in_path, in_layer, out_path, out_layer='result',
                 destination='destination', nodes='nodes'):
        self.in_path = in_path
        self.in_layer = in_layer
        self.out_path = out_path
        self.out_layer = out_layer
        self.destination = destination
        self.nodes = nodes

    def _copy_destination(self):
        cmd = ['ogr2ogr', '-f', 'GPKG', self.out_path, '-append',
               self.in_path, 'destination']
        return subprocess.check_output(cmd)

    def _copy_nodes(self):
        cmd = ['ogr2ogr', '-f', 'GPKG', self.out_path, '-append', self.in_path,
               'nodes']
        return subprocess.check_output(cmd)

    def _read_data(self):
        return gpd.read_file(self.in_path, layer=self.in_layer)

    def _sanitize_data(self, gdf):
        gdf = gdf.where(pd.notnull(gdf), None)
        gdf['blts_score'] = 0
        gdf['plts_score'] = 0
        gdf['alts_score'] = 0
        return gdf

    def _to_geopackage(self, gdf):
        gdf.to_file(self.out_path, layer=self.out_layer, driver="GPKG")

    def calculate_lts(self):
        self._copy_nodes()
        self._copy_destination()
        gdf = self._sanitize_data(self._read_data())

        bar = Bar('Caculating LTS Score...', max=len(gdf))
        for index, row in gdf.iterrows():
            bar.next()
            segment = Segment(
                    lanes_per_direction=row['lanes_per_direction'],
                    parking_lane_width=row['parking_lane_width'],
                    aadt=row['aadt'],
                    functional_classification=row['functional_classification'],
                    posted_speed=row['posted_speed'],
                    total_lanes=row['total_lanes'],
                    overall_landuse=row['overall_land_use'],
                    heavy_vehicle_count=row['heavy_vehicle_count'],
                    pavement_condition=row['pavement_condition'],
                    volume_capacity=row['volume_capacity'],
                    bus_trips_total=row['bus_trips_total'])
            approaches = [Approach(
                    lane_configuration=row['lane_configuration'],
                    right_turn_lane_length=row['right_turn_length'],
                    bicycle_approach_alignment=row[
                        'bicycle_approach_alignment'])]
            crossings = [Crossing(
                crossing_speed=row['crossing_speed'],
                lanes=row['max_lanes_crossed'],
                control_type=row['intersection_control_type'],
                median=row['intersection_median_refuge'],
                functional_classification=row[
                    'crossing_functional_classification'],
                aadt=row['crossing_aadt'])]
            bike_paths = [BikePath(
                    width=row['bicycle_facility_width'],
                    path_category=row['bicycle_path_category'],
                    buffer_width=row['bicycle_buffer_width'],
                    buffer_type=row['bicycle_buffer_type'],
                    path_type=row['bicycle_path_type'])]
            sidewalks = [Sidewalk(
                    sidewalk_width=row['sidewalk_width'],
                    buffer_type=row['sidewalk_buffer_type'],
                    buffer_width=row['sidewalk_buffer_width'],
                    sidewalk_score=row['sidewalk_condition_score'])]
            intersections = [Intersection()]
            railroad_crossing = RailroadCrossing(
                    xing_type=row['railroad_crossing_type'])
            sign = Sign(sign_type=row['road_sign_type'])

            blts_score = segment.blts_score(approaches,
                                            crossings,
                                            bike_paths)
            plts_score = segment.plts_score(crossings, sidewalks)
            alts_score = segment.alts_score(intersections,
                                            bike_paths,
                                            railroad_crossing,
                                            sign,
                                            approaches)
            # assign score
            gdf.loc[index, 'blts_score'] = int(blts_score)
            gdf.loc[index, 'plts_score'] = int(plts_score)
            gdf.loc[index, 'alts_score'] = int(alts_score)
        bar.finish()
        self._to_geopackage(gdf)
