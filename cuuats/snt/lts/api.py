import sys
import fiona
from cuuats.snt.lts import (
    Segment,
    Approach,
    BikePath,
    Crossing,
    Sidewalk,
    Intersection,
    RailroadCrossing,
    Sign,
)
from progress.bar import Bar
import subprocess
import geopandas as gpd


def modify_schema(schema):
    schema["properties"]["blts_score"] = "int"
    schema["properties"]["alts_score"] = "int"
    schema["properties"]["plts_score"] = "int"
    schema["geometry"] = "LineString"
    return schema


def calculate_lts(in_path, in_layer, out_path, out_layer, driver="GPKG"):
    records = []
    with fiona.open(in_path, "r", layer=in_layer) as src:
        schema = modify_schema(src.schema)
        crs = src.crs
        bar = Bar("Caculating LTS Score...", max=len(src))
        for f in src:
            data = f["properties"]
            segment = Segment(
                lanes_per_direction=data["lanes_per_direction"],
                parking_lane_width=data["parking_lane_width"],
                aadt=data["aadt"],
                functional_classification=data["functional_classification"],
                posted_speed=data["posted_speed"],
                total_lanes=data["total_lanes"],
                overall_landuse=data["overall_landuse"],
                heavy_vehicle_count=data["heavy_vehicle_count"],
                pavement_condition=data["pavement_condition"],
                volume_capacity=data["volume_capacity"],
                bus_trips_total=data["bus_trips_total"],
            )
            approaches = [
                Approach(
                    lane_configuration=data["lane_configuration"],
                    right_turn_lane_length=data["right_turn_length"],
                    bicycle_approach_alignment=data[
                        "bicycle_approach_alignment"
                    ],
                )
            ]
            crossings = [
                Crossing(
                    crossing_speed=data["crossing_speed"],
                    lanes=data["max_lanes_crossed"],
                    control_type=data["intersection_control_type"],
                    median=data["intersection_median_refuge"],
                    functional_classification=data[
                        "crossing_functional_classification"
                    ],
                    aadt=data["crossing_aadt"],
                )
            ]
            bike_paths = [
                BikePath(
                    width=data["bicycle_facility_width"],
                    path_category=data["bicycle_path_category"],
                    buffer_width=data["bicycle_buffer_width"],
                    buffer_type=data["bicycle_buffer_type"],
                    path_type=data["bicycle_path_type"],
                )
            ]
            sidewalks = [
                Sidewalk(
                    sidewalk_width=data["sidewalk_width"],
                    buffer_type=data["sidewalk_buffer_type"],
                    buffer_width=data["sidewalk_buffer_width"],
                    sidewalk_score=data["sidewalk_condition_score"],
                )
            ]
            intersections = [Intersection()]
            railroad_crossing = RailroadCrossing(
                xing_type=data["railroad_crossing_type"]
            )
            sign = Sign(sign_type=data["road_sign_type"])

            blts_score = segment.blts_score(
                approaches, crossings, bike_paths, 10000
            )
            plts_score = segment.plts_score(crossings, sidewalks)
            alts_score = segment.alts_score(
                intersections, bike_paths, railroad_crossing, sign, approaches
            )

            f["properties"]["blts_score"] = int(blts_score)
            f["properties"]["plts_score"] = int(plts_score)
            f["properties"]["alts_score"] = int(alts_score)
            records.append(f)
            bar.next()
        bar.finish()

    # selecting the worse score from segment
    gdf = gpd.GeoDataFrame.from_features(
        [feature for feature in records], crs=crs
    )
    gdf = gdf[list(schema["properties"].keys()) + ["geometry"]]

    gdf = gdf.sort_values(
        by=["plts_score", "blts_score", "alts_score"], ascending=False
    )
    gdf = gdf.groupby(["segment_id"]).first()
    gdf = gpd.GeoDataFrame(gdf)
    gdf.crs = crs
    gdf = gdf.reset_index()
    gdf.to_file(out_path, layer=out_layer)


def main(in_path, in_layer, out_path=None, out_layer="result"):
    cmd = [
        "ogr2ogr",
        "-f",
        "GPKG",
        out_path,
        "-append",
        in_path,
        "destination",
    ]
    subprocess.check_output(cmd)
    cmd = ["ogr2ogr", "-f", "GPKG", out_path, "-append", in_path, "nodes"]
    subprocess.check_output(cmd)

    if not out_path:
        out_path = in_path
    calculate_lts(in_path, in_layer, out_path, out_layer)


if __name__ == "__main__":
    if len(sys.argv) > 2:
        main(sys.argv[1], sys.argv[2], sys.argv[3])
    else:
        print("Need path for geopackage and in layer name")
