# Segment Class for LTS Assessment
from ..config import conf
from ..utils import calculate_score
import math
from .zoning import Zoning


class Segment(object):
    def __init__(self, **kwargs):
        self.lanes_per_direction = kwargs.get("lanes_per_direction") or 0
        self.parking_lane_width = kwargs.get("parking_lane_width")
        self.aadt = int(kwargs.get("aadt") or 0)
        self.functional_classification = self._categorize_functional_class(
            kwargs.get("functional_classification")
        )
        self.interstate = self._is_interstate(
            kwargs.get("functional_classification")
        )
        self.posted_speed = kwargs.get("posted_speed") or 0
        self.total_lanes = kwargs.get("total_lanes") or 0
        self.marked_center_lane = kwargs.get("marked_center_lane")
        self.overall_landuse = kwargs.get("overall_landuse") or None
        self.heavy_vehicle_count = kwargs.get("heavy_vehicle_count") or 0
        self.in_school_zone = False
        self.has_railroad_crossing = False
        self.pavement_condition = kwargs.get("pavement_condition") or None
        self.street_width = kwargs.get("street_width") or None
        self.volume_capacity = kwargs.get("volume_capacity") or 0
        self.bus_trips_total = kwargs.get("bus_trips_total") or 0

    def _is_interstate(self, functional_classification):
        if functional_classification == 1:
            return True
        return False

    def _categorize_functional_class(self, functional_classification):
        fc = conf.functional_class_lookup.get(functional_classification)
        if fc is None:
            return "C"
        if fc >= 4:
            return "C"
        else:
            return "A"

    def _find_off_street_trail(self, bike_path):
        trail = conf.off_street_trail
        if trail.get(bike_path.path_category) == "Off-Street Trail":
            return True
        return False

    def _calculate_mix_traffic(self):
        """
        this function calculate the mix traffic scores based on the specify
        criterias
        :param self: self
        :return: np.int64 score
        """
        aadt = self.aadt
        lpd = self.lanes_per_direction
        table = conf.mixed_traffic_table
        aadt_scale = conf.urban_fix_traffic_aadt_scale
        lane_scale = conf.urban_fix_traffic_lane_scale

        crits = ([aadt, aadt_scale], [lpd, lane_scale])
        return calculate_score(table, crits)

    def _calculate_bikelane_with_adj_parking(self, bike_path):
        parking_lane_width = self.parking_lane_width
        lpd = self.lanes_per_direction
        aadt = self.aadt
        one_lane_table = conf.bl_adj_pk_table_one_lane
        multi_lane_table = conf.bl_adj_pk_table_two_lanes
        aadt_scale = conf.bl_adj_pk_aadt_scale
        width_scale = conf.bl_adj_pk_width_scale
        width_scale_two_lanes = conf.bl_adj_pk_two_width_scale
        width = bike_path.width

        score = float("Inf")
        # if there is no bike lane width or no bike lane
        if parking_lane_width is None or width is None:
            return score

        # No marked lanes or 1 lpd
        elif lpd is None or lpd == 1:
            crits = (
                [aadt, aadt_scale],
                [width + parking_lane_width, width_scale],
            )
            return calculate_score(one_lane_table, crits)
        # 2 lpd or greater
        else:
            crits = (
                [aadt, aadt_scale],
                [width + parking_lane_width, width_scale_two_lanes],
            )
            return calculate_score(multi_lane_table, crits)

    def _calculate_bikelane_without_adj_parking(self, bike_path):
        parking_lane_width = self.parking_lane_width
        lpd = self.lanes_per_direction
        aadt = self.aadt
        one_lane_table = conf.bl_no_adj_pk_table_one_lane
        multi_lane_table = conf.bl_no_adj_pk_table_two_lanes
        aadt_scale = conf.bl_no_adj_pk_aadt_scale
        width_scale_one_lane = conf.bl_no_adj_pk_width_scale
        width_scale_two_lane = conf.bl_no_adj_pk_two_width_scale
        width = bike_path.width
        buffer_width = bike_path.buffer_width

        score = 99
        if parking_lane_width is not None or width is None:
            return score
        # no marked lane or 1 lpd
        elif lpd is None or lpd == 1:
            crits = (
                [aadt, aadt_scale],
                [width + buffer_width, width_scale_one_lane],
            )
            return calculate_score(one_lane_table, crits)
        # 2 lps or greater
        else:
            crits = (
                [aadt, aadt_scale],
                [width + buffer_width, width_scale_two_lane],
            )
            return calculate_score(multi_lane_table, crits)

    def _calculate_right_turn_lane(self, approach):
        lane_config = approach.lanes.config
        rtl_length = approach.right_turn_lane_length
        bicycle_approach_alignment = approach.bicycle_approach_alignment
        functional_classification = self.functional_classification
        rtl_score = conf.right_turn_lane_table
        straight = "Straight"
        R = "R"  # right turn lane
        Q = "Q"  # dual shared right turn

        score = 0
        if lane_config is None or functional_classification == "C":
            return score

        if R in lane_config:
            if rtl_length <= 150 and bicycle_approach_alignment is straight:
                return rtl_score[0]
            elif rtl_length > 150 and bicycle_approach_alignment is straight:
                return rtl_score[1]
            else:
                return rtl_score[2]
        elif Q in lane_config:
            return rtl_score[3]

        return score

    def _calculate_left_turn_lane(self, approach):
        speed = self.posted_speed
        functional_classification = self.functional_classification
        K = "K"  # dual shared
        L = "L"  # exclusive left turn lane
        dual_share_table = conf.left_turn_lane_dual_shared_table
        ltl_table = conf.left_turn_lane_table
        lane_crossed = approach.lanes.lanes_crossed
        speed_scale = conf.left_turn_lane_dual_shared_speed_scale
        lane_crossed_scale = conf.left_turn_lane_lane_crossed_scale

        if approach.lanes.config is None or functional_classification == "C":
            return 0
        if L in approach.lanes.config or K in approach.lanes.config:
            crit = [(speed, speed_scale)]
            return calculate_score(dual_share_table, crit)
        else:
            crit = [(speed, speed_scale), (lane_crossed, lane_crossed_scale)]

            return calculate_score(ltl_table, crit)

    def _calculate_crossing_without_median(self, crossing):
        speed = crossing.crossing_speed
        lanes_crossed = crossing.lanes
        crossing_no_med_table = conf.crossing_no_median_table
        speed_scale = conf.crossing_no_median_speed_scale
        lane_scale = conf.crossing_no_median_lane_scale

        crit = [(speed, speed_scale), (lanes_crossed, lane_scale)]

        return calculate_score(crossing_no_med_table, crit)

    def _calculate_crossing_with_median(self, crossing):
        speed = crossing.crossing_speed
        lanes_crossed = crossing.lanes
        crossing_no_med_table = conf.crossing_has_median_table
        speed_scale = conf.crossing_has_median_speed_scale
        lane_scale = conf.crossing_has_median_lane_scale

        crit = [(speed, speed_scale), (lanes_crossed, lane_scale)]

        return calculate_score(crossing_no_med_table, crit)

    def _calculate_condition_score(self, sidewalk):
        width = sidewalk.sidewalk_width
        cond = sidewalk.sidewalk_score
        table = conf.sidewalk_condition_table
        width_scale = conf.sidewalk_condition_width_scale
        cond_scale = conf.sidewalk_condition_condition_scale
        no_sidewalk_score = 4

        # PLTS 4 if there is no sidewalk
        if width is None or cond is None:
            return no_sidewalk_score

        crits = ([width, width_scale], [cond, cond_scale])

        return calculate_score(table, crits)

    def _calculate_buffer_type_score(self, sidewalk):
        buffer_type = conf.buffer_type_lookup.get(
            sidewalk.buffer_type, "no buffer"
        )
        speed = self.posted_speed
        table = conf.buffer_type_table
        buffer_scale = conf.buffer_type_type_scale
        speed_scale = conf.buffer_type_speed_scale

        crits = ([buffer_type, buffer_scale], [speed, speed_scale])

        return calculate_score(table, crits)

    def _calculate_buffer_width_score(self, sidewalk):
        total_lanes = self.total_lanes
        buffer_width = sidewalk.buffer_width
        width_scale = conf.buffer_width_width_scale
        lane_scale = conf.buffer_width_lane_scale
        table = conf.buffer_width_table
        crits = ([total_lanes, lane_scale], [buffer_width, width_scale])

        return calculate_score(table, crits)

    def _calculate_landuse_score(self, type):
        # TODO: get landuse score from a dictionary
        # return conf.LANDUSE_DICT.get(self.overall_landuse)
        zone = Zoning()
        if type == "plts":
            return zone.get_plts(self.overall_landuse) or 0
        else:
            raise TypeError(type + " is not a valid type")
        # return the overall value in database as score
        # if self.overall_landuse == '0':
        #     return 1
        # return int(self.overall_landuse)

    def _calcualate_collector_crossing_wo_med(self, crossing):
        speed = crossing.crossing_speed
        lanes_crossed = crossing.lanes
        crossing_table = conf.collector_crossing_table
        speed_scale = conf.collector_crossing_speed_scale
        lane_scale = conf.collector_crossing_lane_scale

        crit = [(speed, speed_scale), (lanes_crossed, lane_scale)]

        return calculate_score(crossing_table, crit)

    def _calculate_art_crossing_wo_med_two_lanes(self, crossing):
        speed = crossing.crossing_speed
        lanes_crossed = crossing.aadt
        crossing_table = conf.arterial_crossing_two_lanes_table
        speed_scale = conf.arterial_crossing_speed_scale
        lane_scale = conf.arterial_crossing_two_lanes_aadt_scale

        crit = [(speed, speed_scale), (lanes_crossed, lane_scale)]

        return calculate_score(crossing_table, crit)

    def _calculate_art_crossing_wo_med_three_lanes(self, crossing):
        speed = crossing.crossing_speed
        lanes_crossed = crossing.aadt
        crossing_table = conf.arterial_crossing_three_lanes_table
        speed_scale = conf.arterial_crossing_speed_scale
        lane_scale = conf.arterial_crossing_three_lanes_aadt_scale

        crit = [(speed, speed_scale), (lanes_crossed, lane_scale)]

        return calculate_score(crossing_table, crit)

    def _vertical_score(self, bike_path, score):
        if bike_path.buffer_type == conf.vertical_trail:
            if score is float("Inf"):
                return score
            elif score > 1:
                return score - 1
        return score

    def _calculate_on_street_path_score(self, bike_path):
        category = bike_path.path_category
        buffer_width = bike_path.buffer_width
        path_type = bike_path.path_type

        on_street = "On-Street Bikeway"
        sharrows = "Sharrows"

        if category != on_street:
            return conf.on_street_facility.get("no_facility")

        if buffer_width > 0:
            return conf.on_street_facility.get("buffered")

        if path_type != sharrows:
            return conf.on_street_facility.get("non_buffered")

        return conf.on_street_facility.get("sharrows")

    def _calculate_heavy_vehicle_score(self):
        aadt = self.aadt
        hvc = self.heavy_vehicle_count
        ratio_scale = conf.heavy_vehicle_scale
        heavy_vehicle_table = conf.heavy_vehicle_table

        if aadt is None or hvc is None or aadt == 0:
            return 1

        ratio = hvc / aadt
        crit = [(ratio, ratio_scale)]
        return calculate_score(heavy_vehicle_table, crit)

    def _find_school_zone(self, sign):
        if sign.sign_type in conf.school_sign_type:
            self.in_school_zone = True

    def _find_railroad_crossing(self, railroad_crossing):
        if railroad_crossing.xing_type == conf.crossing_type:
            self.has_railroad_crossing = True

    def _find_turn_lanes(self, approach):
        if "L" in approach.lanes.config or "R" in approach.lanes.config:
            return True

    def _turn_lane_calming(self, approaches):
        for approach in approaches:
            if approach.lanes.config is None or approach.lanes.config == 0:
                continue
            if (
                self._find_turn_lanes(approach)
                and self.lanes_per_direction >= 2
            ):
                return True
            return False

    def _calculate_bus_trips_score(self):
        trips_count = self.bus_trips_total
        bus_trips_table = conf.bus_trips_table
        bus_trips_scale = conf.bus_trips_scale
        crit = [(trips_count, bus_trips_scale)]
        return calculate_score(bus_trips_table, crit)

    def _calculate_pavement_condition_score(self):
        pavement_condition = self.pavement_condition
        pavement_condition_table = conf.pavement_condition_table
        pavement_condition_scale = conf.pavement_condition_scale

        if pavement_condition is None:
            return conf.pavement_condition_none_score

        crit = [(pavement_condition, pavement_condition_scale)]
        return calculate_score(pavement_condition_table, crit)

    def _calculate_lane_width_score(self):
        if self.street_width is None:
            return conf.street_width_none_score

        if self.total_lanes == 0:
            width = self.street_width
        else:
            width = self.street_width / self.total_lanes

        width_table = conf.street_width_table
        width_scale = conf.street_width_scale

        crit = [(width, width_scale)]
        return calculate_score(width_table, crit)

    def _calculate_volume_capacity_score(self):
        volume_capacity = self.volume_capacity
        vc_table = conf.volume_capacity_table
        vc_scale = conf.volume_capacity_scale

        if volume_capacity == 0:
            return conf.volume_capacity_none_score

        crit = [(volume_capacity, vc_scale)]
        return calculate_score(vc_table, crit)

    def _round(self, f):
        if f - int(f) >= 0.5:
            return math.ceil(f)
        else:
            return math.floor(f)

    def alts_score(
        self,
        intersections=None,
        bike_paths=None,
        railroad_crossing=None,
        sign=None,
        approaches=None,
    ):
        segment_score = 1
        path_score = 1

        for bike_path in bike_paths:
            path_score = max(
                path_score, self._calculate_on_street_path_score(bike_path)
            )

        bus_trips_score = self._calculate_bus_trips_score()
        heavy_vehicle_score = self._calculate_heavy_vehicle_score()
        pavement_condition_score = self._calculate_pavement_condition_score()
        lane_width_score = self._calculate_lane_width_score()
        volume_capacity_score = self._calculate_volume_capacity_score()
        self._find_school_zone(sign)
        self._find_railroad_crossing(railroad_crossing)

        score_components = [
            segment_score,
            path_score,
            heavy_vehicle_score,
            bus_trips_score,
            pavement_condition_score,
            lane_width_score,
            volume_capacity_score,
        ]
        score = max([s for s in score_components if s != 0])

        if self.in_school_zone and score < 4:
            score = score + conf.school_zone_score
        if self.has_railroad_crossing and score < 4:
            score = score + conf.railroad_crossing_score
        if self._turn_lane_calming(approaches):
            score = score - conf.vehicle_turn_lane_score
        return float(score)

    def blts_score(
        self,
        approaches,
        crossings,
        bike_paths=None,
        turn_threshold=conf.turn_threshold,
    ):

        # Interstate Score
        if self.interstate:
            return 4

        rtl_score = 0
        ltl_score = 0
        pk_score = 0
        no_pk_score = 0

        mix_traffic_score = self._calculate_mix_traffic()

        for bike_path in bike_paths:
            if self._find_off_street_trail(bike_path):
                return conf.off_street_trail_score

            # adds vertical logic
            p = self._vertical_score(
                bike_path, self._calculate_bikelane_with_adj_parking(bike_path)
            )
            pk_score = max(pk_score, p)

            # adds vertical logic
            n = self._vertical_score(
                bike_path,
                self._calculate_bikelane_without_adj_parking(bike_path),
            )
            no_pk_score = max(no_pk_score, n)

        segment_components = [mix_traffic_score, pk_score, no_pk_score]

        segment_score = min([s for s in segment_components if s is not 0])

        if self.aadt >= turn_threshold:
            # turn lane criteria
            for approach in approaches:
                rtl_score = max(
                    rtl_score, self._calculate_right_turn_lane(approach)
                )
                ltl_score = max(
                    ltl_score, self._calculate_left_turn_lane(approach)
                )

        # crossing criteria
        for crossing in crossings:
            if crossing.control_type == "Signal":
                crossing_score = 1
            else:
                if crossing.median is None:
                    crossing_score = self._calculate_crossing_without_median(
                        crossing
                    )
                else:
                    crossing_score = self._calculate_crossing_with_median(
                        crossing
                    )

        score_components = [
            segment_score,
            rtl_score,
            ltl_score,
            crossing_score,
        ]

        return max([s for s in score_components if s is not 0])

    def plts_score(self, crossings, sidewalks=None):
        segment_score = float("Inf")
        for sidewalk in sidewalks:
            cond_score = self._calculate_condition_score(sidewalk)
            buffer_type_score = self._calculate_buffer_type_score(sidewalk)
            buffer_width_score = self._calculate_buffer_width_score(sidewalk)
            landuse_score = self._calculate_landuse_score("plts")
            sidewalk_score = max(
                cond_score,
                buffer_type_score,
                buffer_width_score,
                landuse_score,
            )

            segment_score = min(segment_score, sidewalk_score)
            if (
                sidewalk.sidewalk_width is None
                or sidewalk.sidewalk_score is None
            ):
                return sidewalk_score
            for crossing in crossings:
                if crossing.control_type == "Signal":
                    crossing_score = 1
                    continue
                # median criteria - no median
                if crossing.median is None:
                    # collector crossing
                    if crossing.functional_classification >= 4:
                        crossing_score = self._calcualate_collector_crossing_wo_med(
                            crossing
                        )
                    # arterial crossing
                    else:
                        if crossing.lanes <= 2:
                            crossing_score = self._calculate_art_crossing_wo_med_two_lanes(
                                crossing
                            )
                        else:
                            crossing_score = self._calculate_art_crossing_wo_med_three_lanes(
                                crossing
                            )
                # median criteria - median present
                else:
                    raise NotImplementedError(
                        "Can only have crossing without median"
                    )

        return max(segment_score, crossing_score)
