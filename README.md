# cuuats.snt.lts
Python-based tool for calculating the Bike Level Traffic of Stress (BLTS),
Pedestrian Level Traffic of Stress (PLTS), Automobile Level of Stress (ALTS)

This analysis tool follows the documentation for Low-Stress Bicycling and
Network Connectivity Report from Mineta Transportation Institute and Bicycle
Level of Traffic Stress and Pedestrian Level of Traffic Stress from report of
Oregon Department of Transportation.


## Development
To run the tests:
```
python -m unittest discover
```

## Documentation
The following contains the documentation for the cuuats.snt.lts

### Data Structure
In order to generate the BLTS, PLTS, ALTS Score, the LTS tool expects the data in the following format.

|  Name  |  Data Type |  Domain | Usage  |
| --- | --- | --- | --- |
| segment_id | interger | | |
| name | text | | |
| cross_name_start | text | | |
| cross_name_end | text | | |
| geom | geom | | |
| aadt | integer | |  BLTS, ALTS |
| bicycle_facility_width | interger | inches | BLTS |
| bicycle_path_category | text | On-Street Bikeway, Off-Street Trail | BLTS, ALTS |
| bicycle_buffer_width | integer | inches | BLTS, ALTS  |
| bicycle_buffer_type | text | Vertical, Landscaped, Landscaped with Trees, No Buffer, Not Applicable | BLTS |
| bicycle_path_type | text | Sharrows, Shared-Use Path, Bike Lanes | ALTS |
| bicycle_approach_alignment | text | Straight, End, Drop, Left | BLTS |
| bus_trips_total | integer | | ALTS |
| crossing_aadt | integer | | PLTS |
| crossing_functional_classification | integer | [Functional Classification](#functional-classification) | PLTS |
| crossing_speed | integer | | BLTS, PLTS |
| functional_classification | interger  | [Functional Classification](#functional-classification) | BLTS |
| heavy_vehicle_count | integer | | ALTS |
| intersection_control_type | text | signalized, unsignalized | BLTS, PLTS |
| intersection_median_refuge | boolean | True, False |  BLTS, PLTS |
| lane_configuration | text | [Lane Configuration Table](#lane-configuration-table) | BLTS, ALTS |
| lanes_per_direction | integer | | BLTS, ALTS |
| max_lanes_crossed | integer | | BLTS, PLTS |
| overall_land_use | text |  | PLTS  |
| parking_lane_width | integer | | BLTS |
| pavement_condition | text | Failed, Poor, Fair, Good, Excellent | ALTS |
| posted_speed | integer | | BLTS, PLTS |
| railroad_crossing_type | text | PUB | ALTS |
| right_turn_length | integer | | BLTS |
| road_sign_type | text | S5-1, S5-2 | ALTS |
| sidewalk_buffer_width | integer | | PLTS |
| sidewalk_condition_score | integer | 0-100 | PLTS |
| sidewalk_width | integer | inches | PLTS |
| sidewalk_buffer_type |  text | no buffer, solid surface, landscaped, landscaped with tree, solid surface | PLTS |
| total_lanes | integer |  | PLTS |
| volume_capacity | float | | ALTS  |
| zone_code | text | | ALTS, PLTS |


#### Lane configuration table
| Character | Type |
| --- | --- |
| X | Oncoming Traffic (leaving the intersection) |
| K | Shared left-turn lane |
| L | Exclusive left-turn lane |
| M | Two-way left-turn lane |
| T | Through lane (exclusive or unmarked shared) |
| R | Exclusive right-turn Lane |
| Q | Shared right-turn lane |

#### Functional Classification
| Number | Category |
| --- | --- |
| 1 | 'Interstate' |
| 2 | 'Other Freeways or Expressways' |
| 3 | 'Other Principal Arterial' |
| 4 | 'Minor Arterial' |
| 5 | 'Major Collector' |
| 6 | 'Minor Collector' |
| 7 | 'Local' |

### BLTS (Bicycle Level of Traffic Stress)
Bicycle Level of Traffic Stress measures the stress of bicyclists experiences based on different roadway conditions.

#### Segment Criteria
A segment falls within either one of the following categories. If two conditions exist at the same time (bike lane present and mix traffic). Both situations are evaluated and the worst of the two scores is used to represent the segment.

<table>
    <thead>
    **Bike Lane with Adjacent Parking Lane Criteria**
        <tr>
            <th></th>
            <th></th>
            <th colspan='3'>1 lane per direction</th>
            <th colspan='2'>>= 2 lanes per direction</th>
        </tr>
        <tr>
            <th>Posted Speed</th>
            <th>AADT</th>
            <th>≥ 180” bike lane + parking</th>
            <th>157” - 179” bike lane + parking </th>
            <th>≤ 156” bike lane + parking </th>
            <th>≥ 180” bike lane + parking </th>
            <th>≤ 179” bike lane + parking</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>⇐ 25 mph</td>
            <td>0-1,000 </td>
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>2</td>
            <td>3</td>
        </tr>
            <td>⇐ 30 mph</td>
            <td>1,001 - 3,000</td>
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>2</td>
            <td>3</td>
        </tr>
        <tr>
            <td>⇐ 35 mph</td>
            <td>3,001 - 30,000 </td>
            <td>2</td>
            <td>3</td>
            <td>3</td>
            <td>3</td>
            <td>3</td>
        </tr>
        <tr>
            <td>>= 40 mph </td>
            <td>>30,000</td>
            <td>2</td>
            <td>4</td>
            <td>4</td>
            <td>3</td>
            <td>4</td>
        </tr>
    </tbody>
</table>

<table>
    <thead>
    **Bike lane without adjacent parking lane**
        <tr>
            <th></th>
            <th></th>
            <th colspan='3'>1 lane per direction</th>
            <th colspan='2'>>= 2 lanes per direction</th>
        </tr>
        <tr>
            <th>Posted Speed</th>
            <th>AADT</th>
            <th>≥ 180” bike lane + parking</th>
            <th>157” - 179” bike lane + parking </th>
            <th>≤ 156” bike lane + parking </th>
            <th>≥ 180” bike lane + parking </th>
            <th>≤ 179” bike lane + parking</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>⇐30 mph</td>
            <td>⇐3,000</td>
            <td>1</td>
            <td>1</td>
            <td>2</td>
            <td>1</td>
            <td>3</td>
        </tr>
            <td>⇐ 35 mph</td>
            <td>3,001 - 30,000</td>
            <td>2</td>
            <td>3</td>
            <td>3</td>
            <td>2</td>
            <td>3</td>
        </tr>
        <tr>
            <td>>= 40 mph</td>
            <td>>30,000</td>
            <td>3</td>
            <td>4</td>
            <td>4</td>
            <td>3</td>
            <td>4</td>
        </tr>
    </tbody>
</table>

<table>
    <thead>
    **Mix traffic**
        <tr>
            <th>Posted Speed</th>
            <th>AADT</th>
            <th>≥ 180” bike lane + parking</th>
            <th>157” - 179” bike lane + parking </th>
            <th>≤ 156” bike lane + parking </th>
            <th>≥ 180” bike lane + parking </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>⇐30 mph</td>
            <td>⇐3,000</td>
            <td>1</td>
            <td>1</td>
            <td>2</td>
            <td>1</td>
        </tr>
            <td>⇐ 35 mph</td>
            <td>3,001 - 30,000</td>
            <td>2</td>
            <td>3</td>
            <td>3</td>
            <td>2</td>
        </tr>
        <tr>
            <td>>= 40 mph</td>
            <td>>30,000</td>
            <td>3</td>
            <td>4</td>
            <td>4</td>
            <td>3</td>
        </tr>
    </tbody>
</table>

#### Turn Lane Criteria
If turn lanes are presented at either end of the approach, the turn lanes criteria are considered. The worst turn lane score is applied to the segment.

The user can change the configuration has an option to include or exclude the turn lane criteria based on aadt. The default is 10,000 aadt count before the turn lane criteria are considered.

<table>
    <thead>
    **Right turn lane criteria**
        <tr>
            <th>Right-turn lane configuration</th>
            <th>Right-turn lane length(ft)</th>
            <th>Bike lane approach alignment </th>
            <th>LTS Score </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Single</td>
            <td>⇐ 150</td>
            <td>Straight</td>
            <td>2</td>
        </tr>
            <td>Single</td>
            <td>> 150</td>
            <td>Straight</td>
            <td>3</td>
        </tr>
        <tr>
            <td>Single</td>
            <td>Any</td>
            <td>Left</td>
            <td>3</td>
        </tr>
        <tr>
            <td>Single or Dual Exclusive/Shared</td>
            <td>Any</td>
            <td>Any</td>
            <td>4</td>
        </tr>
    </tbody>
</table>

<table>
    <thead>
    **Left turn lane criteria**
        <tr>
            <th>Speed Limit</th>
            <th>No lane crossed</th>
            <th>1 lane crossed</th>
            <th>2+ lanes crossed</th>
            <th>Dual shared or exclusive left turn lane</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>⇐ 25 mph</td>
            <td>2</td>
            <td>2</td>
            <td>3</td>
            <td>4</td>
        </tr>
            <td>⇐ 30 mph</td>
            <td>2</td>
            <td>3</td>
            <td>4</td>
            <td>4</td>
        </tr>
        <tr>
            <td>⇐ 35 mph</td>
            <td>3</td>
            <td>4</td>
            <td>4</td>
            <td>4</td>
        </tr>
    </tbody>
</table>

#### Crossing Criteria

Crossing criteria evaluate the attribute of the street being crossed at each intersection of the segment. Signalized intersection does not add to the stress level of the segment. Unsignalized intersections are evaluated with the following criteria. If the stress score is greater than the segment or turn-lane score. The crossing score will be used to represent the segment.

<table>
    <thead>
    **Un-signalized Intersection Crossing Without a Median Refuge Criteria**
        <tr>
            <th rowspan=2>Speed Limit</th>
            <th colspan=3>Total Lanes Crossed (Both Directions) </th>
        </tr>
        <tr>
            <th><3 lanes</th>
            <th>4-5 lanes</th>
            <th>>=6 lanes</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>⇐ 25 mph</td>
            <td>1</td>
            <td>2</td>
            <td>4</td>
        </tr>
            <td>30 mph</td>
            <td>1</td>
            <td>2</td>
            <td>4</td>
        </tr>
        <tr>
            <td>35 mph</td>
            <td>2</td>
            <td>3</td>
            <td>4</td>
        </tr>
        <tr>
            <td>>=40 mph</td>
            <td>3</td>
            <td>4</td>
            <td>4</td>
        </tr>
    </tbody>
</table>

<table>
    <thead>
    **Un-signalized Intersection Crossing With a Median Refuge Criteria**
        <tr>
            <th rowspan=2>Speed Limit</th>
            <th colspan=3>Maximum Through/Turn Lanes Crossed Per Direction</th>
        </tr>
        <tr>
            <th>1-2 lanes</th>
            <th>3-4 lanes</th>
            <th>>5 lanes</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>⇐ 25 mph</td>
            <td>1</td>
            <td>2</td>
            <td>4</td>
        </tr>
            <td>30 mph</td>
            <td>1</td>
            <td>2</td>
            <td>4</td>
        </tr>
        <tr>
            <td>35 mph</td>
            <td>2</td>
            <td>3</td>
            <td>4</td>
        </tr>
        <tr>
            <td>>=40 mph</td>
            <td>3</td>
            <td>4</td>
            <td>4</td>
        </tr>
    </tbody>
</table>

### PLTS
The Pedestrian Level of Traffic Stress measures the stress experienced by pedestrian based on roadway and sidewalk conditions.

#### Sidewalk Condition
The sidewalk condition criteria are based on the condition score and sidewalk width from CUUATS sidewalk segment. The break down is as followed.

<table>
    <thead>
    **Sidewalk Condition Criteria**
        <tr>
            <th rowspan=2>Sidewalk Width (inches)</th>
            <th colspan=5>Sidewalk Condition </th>
        </tr>
        <tr>
            <th>Good</th>
            <th>Fair</th>
            <th>Poor</th>
            <th>Very Poor</th>
            <th>No Sidewalk</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>< 48</td>
            <td>4</td>
            <td>4</td>
            <td>4</td>
            <td>4</td>
            <td>4</td>
        </tr>
            <td>>= 48 to < 60</td>
            <td>3</td>
            <td>3</td>
            <td>3</td>
            <td>4</td>
            <td>4</td>
        </tr>
        <tr>
            <td>>= 60 to < 72</td>
            <td>2</td>
            <td>2</td>
            <td>3</td>
            <td>4</td>
            <td>4</td>
        </tr>
        <tr>
            <td>> 72</td>
            <td>1</td>
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>4</td>
        </tr>
    </tbody>
</table>

The condition score is a numeric score from 0 to 100. The scores are categorized based on these categories.

| Acutual Score | Category |
| --- | --- |
| <=50 | Very Poor |
| 51-60 | Poor |
| 61-70 | Fair |
| >70 | Good |

#### Physical Buffer Criteria
Physical buffers were estimated based on the sampling of each neighborhood from aerial photography.

<table>
    <thead>
    **Physical Buffer Type**
        <tr>
            <th rowspan=2>Buffer Type</th>
            <th colspan=4>Posted Speed</th>
        </tr>
        <tr>
            <th>⇐ 25mph</th>
            <th>30mph</th>
            <th>35mph</th>
            <th>>=40mph</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>No buffer</td>
            <td>2</td>
            <td>3</td>
            <td>3</td>
            <td>4</td>
        </tr>
            <td>Solid Surface</td>
            <td>2</td>
            <td>2</td>
            <td>2</td>
            <td>2</td>
        </tr>
        <tr>
            <td>Landscaped</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td>2</td>
        </tr>
        <tr>
            <td>Landscaped with trees</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>2</td>
        </tr>
        <tr>
            <td>Vertical</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>2</td>
        </tr>
    </tbody>
</table>

<table>
    <thead>
    **Total Buffer Width**
        <tr>
            <th rowspan=2>Total number of of travel lanes 	</th>
            <th colspan=5>Total Buffering Width (feet)</th>
        </tr>
        <tr>
            <th>< 5</th>
            <th>>=5 to < 10</th>
            <th>>= 10 to < 15</th>
            <th>>= 15 to < 25</th>
            <th>>= 25</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>2</td>
            <td>2</td>
            <td>2</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
        </tr>
            <td>3</td>
            <td>3</td>
            <td>2</td>
            <td>2</td>
            <td>1</td>
            <td>1</td>
        </tr>
        <tr>
            <td>4-5</td>
            <td>4</td>
            <td>3</td>
            <td>2</td>
            <td>1</td>
            <td>1</td>
        </tr>
        <tr>
            <td>6</td>
            <td>4</td>
            <td>4</td>
            <td>3</td>
            <td>2</td>
            <td>2</td>
        </tr>
    </tbody>
</table>

#### General Land Use Criteria

| PLTS | Overall Land Use |
| --- | --- |
| 1 | 	Residential, Central Business District, Neighborhood Commercial, Public Space, Office, Mixed Used  |
| 2 | General Commercial, Agricultural |
| 3 | Auto Commercial, Light Industrial |
| 4 | Heavy Industrial |


#### Crossing Criteria
Street segment is classified into arterial and collector/local based on their functional classification.

| Functional classification | Category |
| --- | --- |
| 1, 2, 3 | Arterial |
| 4, 5, 6, 7 | Collector |
| 1 | Interstate |

<table>
    <thead>
    **Collector & Local Unsignalized Intersection Crossing**
        <tr>
            <th rowspan=2>Posted Speed</th>
            <th colspan=5>Total Lanes Crossed</th>
        </tr>
        <tr>
            <th>1 lane</th>
            <th>2 lanes</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>⇐ 25mph</td>
            <td>1</td>
            <td>2</td>
        </tr>
            <td> 30mph </td>
            <td>1</td>
            <td>2</td>
        </tr>
        <tr>
            <td>35mph</td>
            <td>2</td>
            <td>2</td>
        </tr>
        <tr>
            <td>>= 40mph </td>
            <td>3</td>
            <td>3</td>
        </tr>
    </tbody>
</table>

<table>
    <thead>
    **Arterial Unsignalized Intersection Crossing Without a Median Refuge**
        <tr>
          <th rowspan=3>Posted Speed</th>
          <th colspan=6>Total Lanes Crossed</th>
        </tr>
        <tr>
          <th colspan=3>1 lane</th>
          <th colspan=3>2 lanes</th>
        </tr>
        <tr>
          <th>< 5,000 aadt</th>
          <th>5,000-9,000 aadt</th>
          <th>> 9,000 aadt </th>
          <th>< 8,000 aadt</th>
          <th>8,000-12,000 aadt </th>
          <th>> 12,000 aadt</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>< 25mph </td>
            <td>2</td>
            <td>2</td>
            <td>3</td>
            <td>3</td>
            <td>3</td>
            <td>4</td>
        </tr>
            <td>30mph</td>
            <td>2</td>
            <td>3</td>
            <td>3</td>
            <td>3</td>
            <td>3</td>
            <td>4</td>
        </tr>
        <tr>
            <td>35mph</td>
            <td>3</td>
            <td>3</td>
            <td>4</td>
            <td>3</td>
            <td>4</td>
            <td>4</td>
        </tr>
        <tr>
            <td>>= 40mph </td>
            <td>3</td>
            <td>4</td>
            <td>4</td>
            <td>4</td>
            <td>4</td>
            <td>4</td>
        </tr>
    </tbody>
</table>

### ALTS
The Automobile Level of Traffic Stress measures the stress motorists experience according to roadway conditions.

#### On-Street Bicycle Facility Criteria
The present of on-street bicycle facility increases the stress level for motorists based on the following criteria.

|  Bicycle Facility Type  |  Score  |
| --- | --- |
| No facility | 1 |
| Buffered bike lane | 2 |
| No buffered bike lane | 2 |
| Sharrow | 3 |

#### Public Transit Criteria
The interaction for motorists and public transit stops increases stress level for motorist.

| Bus Trip Total | Score |
| --- | --- |
| 0-10,000 | 1 |
| 10,001 - 30,000 | 2 |
| 30,001 - 40,000 | 3 |
| >=40,000 | 4 |

#### Heavy Vehicle
The criteria looks at the ration of AADT count vs. heavy vehicle counts.  It is calculated by using ''%%heavy vehicle count / aadt count%%''

|  Ratio  |  Score  |
| --- | --- |
| 0%-5% | 1 |
| >5%-15% | 2 |
| >15%-30% | 3 |
| >30% | 4 |

#### Pavement Condition Criteria
The input of the pavement condition is categorized.  The categories represent the following scores.

| Category | Score |
| --- | --- |
| Excellent | 1 |
| Good | 1 |
| Fair | 2 |
| Poor | 3 |
| Failed | 4 |

** *Segment without pavement condition score is currently classified as 1. **

#### Volume Capacity Criteria
Volume capacity for each roadway is generated by the TDM model and is matched to the CUUATS segment.  It is a ratio based on the predicted count and the roadway capacity.

| Volume Capacity | Score |
| --- | --- |
| 0-30% | 1 |
| >30% - 60% | 2 |
| >60% - 100% | 3 |
| >100% | 4 |

#### School Zone Criteria
If a school sign is present in the segment(within 35ft), then the school zone criteria apply.  School zone is an addictive criterion, if the segment is within the school zone, a ''%%0.5%%'' score is added to the overall ALTS score.

#### Railroad Crossing Criteria
If a railroad crossing is present in the segment(within 10ft), then the railroad crossing criteria apply. The railroad crossing is an addictive criterion, if the segment has a railroad crossing, a ''%%0.5%%'' score is added to the overall ALTS score.

#### Turn Lane Criteria
If the segment has 2 lanes or more and there is a present of turn-lane, it reduces the stress for motorists.  A score of ''%%0.25%%'' is subtracted from the overall score.
